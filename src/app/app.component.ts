import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsService } from'./Services/utils/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'frontend-buenosAires';
  public productosEnCarrito:number = 0;
  public listaProductos:any[];

  constructor(private router:Router, private utils:UtilsService){}

  ngOnInit(){

    this.utils.sumarProductosCarrito$.subscribe( data => {
      this.productosEnCarrito += data;
    })

    this.utils.restarProductosCarrito$.subscribe( data => {
      this.productosEnCarrito -= data;
    })

    this.listaProductos = JSON.parse(localStorage.getItem("carrito"));
    if(this.listaProductos){
      this.listaProductos.forEach(pro => {
        this.utils.sumarProductosCarrito$.emit(Number(pro.cantidadCarrito));
      });
    }
  }

  irACarroCompras(){
    this.router.navigate(['carro-compras']);
  }
}
