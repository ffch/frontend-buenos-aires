import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { config } from '../../../environments/environment';
import { Producto } from '../../models/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http:HttpClient) { }

  obtenerProductosDesdeBodegaYANWO(): Observable<any>{

    return this.http.get( config.API_BASE_URL_BODEGA + 'productos-vigentes');
  }

  obtenerProductos(): Observable<any>{

    return this.http.get( config.API_BASE_URL_BODEGA + 'productos');
  }

  actualizarProducto(producto:Producto):Observable<any>{

    return this.http.put( config.API_BASE_URL_BODEGA + 'productos/' + producto._id , producto, {observe: 'response'});
  }

  obtenerProductosANWO(): Observable<any>{

    return this.http.get( config.API_BASE_URL_ANWO + 'productos');
  }

  crearProducto(producto:Producto, img:File): Observable<any>{

    const fd = new FormData();
    fd.append('nombre', producto.nombre);
    fd.append('marca', producto.marca);
    fd.append('precio', String(producto.precio));
    fd.append('stock', String(producto.stock));
    fd.append('img', img);

    return this.http.post( config.API_BASE_URL_BODEGA + 'productos', fd, {observe: 'response'});
  }
}
