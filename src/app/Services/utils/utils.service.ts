import { Injectable, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar'

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  public horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  public verticalPosition: MatSnackBarVerticalPosition = 'top';
  public sumarProductosCarrito$ = new EventEmitter<number>();
  public restarProductosCarrito$ = new EventEmitter<number>();
  
  constructor(private spinner:NgxSpinnerService, private snackBar: MatSnackBar) { }

  mostrarSpinner(){
    this.spinner.show(undefined, {
      type: "ball-square-clockwise-spin",
      size: "medium",
      bdColor: "rgba(0, 0, 0, 0.8)",
      color: "white"
    });
  }

  ocultarSpinner(){
    this.spinner.hide();
  }

  formatearFechaCortaParaFront(date) {
		let arrayFecha = date.split('-')
		date = arrayFecha[2] + '-' + arrayFecha[1] + '-' + arrayFecha[0];
		return date;
  }

  formatearFechaCortaParaBack(date) {
		let arrayFecha = date.split('-')
		date = arrayFecha[0] + '-' + arrayFecha[1] + '-' + arrayFecha[2];
		return date;
  }

  messageGood(message: string): void {
		this.snackBar.open(message, 'x', {
			duration: 4000,
			horizontalPosition: this.horizontalPosition,
			verticalPosition: this.verticalPosition,
			panelClass: 'green-snackbar'

		});
  }
  
	messageBad(message: string): void {
		this.snackBar.open(message, 'x', {
			duration: 4000,
			horizontalPosition: this.horizontalPosition,
			verticalPosition: this.verticalPosition,
			panelClass: 'red-snackbar'

		});
  }
}
