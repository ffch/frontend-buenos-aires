import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductosComponent } from './components/productos/productos.component';
import { ServiciosComponent } from './components/servicios/servicios.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { AdministracionProductosComponent } from './components/administracion-productos/administracion-productos.component';
import { AgregarProductoComponent } from './components/administracion-productos/agregar-producto/agregar-producto.component';
import { CarroComprasComponent } from './components/carro-compras/carro-compras.component';


const routes: Routes = [
  { path: '', redirectTo: '/productos', pathMatch: 'full' },
  { path: 'productos', component: ProductosComponent },
  { path: 'servicios', component: ServiciosComponent },
  { path: 'nosotros', component: NosotrosComponent },
  { path: 'adm-productos', component: AdministracionProductosComponent },
  { path: 'adm-productos/agregar-producto', component: AgregarProductoComponent },
  { path: 'carro-compras', component: CarroComprasComponent },
  { path: '**', redirectTo: '/productos' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
