import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../app/material/material.module';
import { NgxSpinnerModule } from "ngx-spinner";

import { AppComponent } from './app.component';
import { ProductosComponent } from './components/productos/productos.component';
import { ServiciosComponent } from './components/servicios/servicios.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { AdministracionProductosComponent } from './components/administracion-productos/administracion-productos.component';
import { AgregarProductoComponent } from './components/administracion-productos/agregar-producto/agregar-producto.component';
import { CarroComprasComponent } from './components/carro-compras/carro-compras.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductosComponent,
    ServiciosComponent,
    NosotrosComponent,
    AdministracionProductosComponent,
    AgregarProductoComponent,
    CarroComprasComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
