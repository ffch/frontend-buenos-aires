export class Producto {
    _id:            string;
    marca:          string;
    nombre:         string;
    precio:         number;
    descripcion:    string;
    imagenPath:     string;
    stock:          number;
    estado:         string;
    fechaCreacion:  string;
}