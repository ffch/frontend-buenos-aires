import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import { Router } from '@angular/router';
import { ProductoService } from '../../Services/producto/producto.service';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { UtilsService } from '../../Services/utils/utils.service';

@Component({
  selector: 'app-administracion-productos',
  templateUrl: './administracion-productos.component.html',
  styleUrls: ['./administracion-productos.component.css']
})
export class AdministracionProductosComponent implements OnInit {

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  //dc = Displayed Columns
  dcProductos: string[] = ['nombre', 'marca', 'precio','descripcion', 'stock','estado', 'editar'];
  //ds = DataSource
  dsProductos:any = [];
  public productos:Array<any>;
  public estados:any[] = [
    {id: 1, descripcion: "Vigente"},
    {id: 2, descripcion: "No Vigente"}
  ];

  public productoForm = new FormGroup({
    marca:            new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
    nombre:           new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(40)]),
    descripcion:      new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
    precio:   new FormControl('', [Validators.required, Validators.min(1)]),
    stock:            new FormControl('', [Validators.required, Validators.min(0)]),
    estado:           new FormControl('', [Validators.required]),
    //fechaCreacion:    new FormControl('', [Validators.required]),
  });

  constructor(private router:Router, private productoService:ProductoService,
    private utils:UtilsService) { }

  ngOnInit(): void {

    this.utils.mostrarSpinner();
    this.productoService.obtenerProductos().subscribe( data => {

      this.productos = data;
      this.productos.forEach(element => {
        element.editar = true;
        //element.fechaCreacion = this.utils.formatearFechaCortaParaFront(element.fechaCreacion);
      });
      console.log('Productos Vigentes', this.productos);

      this.dsProductos = new MatTableDataSource<any>(<any>this.productos);

      this.dsProductos.sort = this.sort;
      this.dsProductos.paginator = this.paginator;

      this.utils.ocultarSpinner();
    }, error => {

      console.log(error);
      this.utils.ocultarSpinner();
    });
  }

  irAgregarProducto():void{

    this.router.navigate(['adm-productos/agregar-producto']);
  }

  get f() { return this.productoForm.controls; }

  abrirEditarProducto(element){
    //element.fechaCreacion = this.utils.formatearFechaCortaParaFront(element.fechaCreacion);
    console.log(element);
    //console.log('PRIMERA VUELTA DE FECHA',element.fechaCreacion);
    this.productoForm.setValue({
      marca:            element.marca,
      nombre:           element.nombre,
      descripcion:      element.descripcion,
      precio:           element.precio,
      stock:            element.stock,
      //fechaCreacion:    element.fechaCreacion,
      estado:           element.estado,
    });

  }

  editarProducto(element){
    this.utils.mostrarSpinner();

    element.marca             = this.productoForm.get('marca').value;
    element.nombre            = this.productoForm.get('nombre').value;
    element.descripcion       = this.productoForm.get('descripcion').value;
    element.precio            = this.productoForm.get('precio').value;
    element.stock             = this.productoForm.get('stock').value;
    element.estado            = this.productoForm.get('estado').value;
    //element.fechaCreacion     = this.utils.formatearFechaCortaParaBack(this.productoForm.get('fechaCreacion').value);
    
    //console.log('SEGUNDA VUELTA DE FECHA', element.fechaCreacion);

    this.actualizarProducto(element);

  }

  actualizarProducto(element){

    console.log('PRODUCTO A ACTUALIZAR',element);
    this.productoService.actualizarProducto(element).subscribe( data => {

      console.log('PRODUCTO ACTUALIZADO',data);
      if(data.status == 201){

        this.productoService.obtenerProductos().subscribe( data => {

          this.productos = data;

          this.productos.forEach(element => {
    
            element.editar = true;
            //element.fechaCreacion = this.utils.formatearFechaCortaParaFront(element.fechaCreacion);
          });
    
          this.dsProductos = new MatTableDataSource<any>(<any>this.productos);
    
          this.dsProductos.sort = this.sort;
          this.dsProductos.paginator = this.paginator;

          this.utils.messageGood("Producto actualizado correctamente.");
          this.utils.ocultarSpinner();

        });
      }else{
        this.utils.messageBad("Error al actualizar un Producto.");
        this.utils.ocultarSpinner();
      }

    }, error => { 
      console.log(error);
      this.utils.messageBad("Error al actualizar un Producto.");
      this.utils.ocultarSpinner();
    });
  }
}
