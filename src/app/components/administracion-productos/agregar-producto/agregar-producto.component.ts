import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { Producto } from 'src/app/models/producto';
import { ProductoService } from '../../../Services/producto/producto.service';
import { UtilsService } from '../../../Services/utils/utils.service';

@Component({
  selector: 'app-agregar-producto',
  templateUrl: './agregar-producto.component.html',
  styleUrls: ['./agregar-producto.component.css']
})
export class AgregarProductoComponent implements OnInit {

  public productoForm = new FormGroup({
    marca:            new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
    nombre:           new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(40)]),
    descripcion:      new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
    precio:           new FormControl('', [Validators.required, Validators.min(1)]),
    stock:            new FormControl('', [Validators.required, Validators.min(1)]),  
    imagen:           new FormControl(null, [Validators.required])
  });
  public imagenSeleccionada:File;
  public producto:Producto = new Producto();

  constructor(
    private router:Router, 
    private productoService:ProductoService, 
    private utils:UtilsService) { }

  ngOnInit(): void {
  }

  get f() { return this.productoForm.controls; }

  irAdministracionProductos(){
    this.router.navigate(['adm-productos']);
  }

  seleccionarImagen(event){
    if(event.target.files && event.target.files[0]){
      this.imagenSeleccionada = event.target.files[0];
    }else{
      this.utils.messageBad("Debe seleccionar una imagen para el producto");
    }
    
  }

  agregarProducto(){
    this.utils.mostrarSpinner();

    this.producto.marca             = this.productoForm.get('marca').value;
    this.producto.nombre            = this.productoForm.get('nombre').value;
    this.producto.descripcion       = this.productoForm.get('descripcion').value;
    this.producto.precio            = this.productoForm.get('precio').value;
    this.producto.stock             = this.productoForm.get('stock').value;

    this.productoService.crearProducto(this.producto, this.imagenSeleccionada).subscribe( data => {

      if(data.status == 200){

        this.utils.messageGood("Producto creado exitosamente");
        this.productoForm.reset();
        this.utils.ocultarSpinner();
      }
    }, error => {
      console.log(error);
      this.utils.ocultarSpinner();
    });
    
  }
}
