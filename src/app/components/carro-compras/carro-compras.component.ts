import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { config } from '../../../environments/environment';
import { UtilsService } from '../../Services/utils/utils.service';
import { ProductoService } from '../../Services/producto/producto.service';

@Component({
  selector: 'app-carro-compras',
  templateUrl: './carro-compras.component.html',
  styleUrls: ['./carro-compras.component.css']
})
export class CarroComprasComponent implements OnInit {

  dcProductos: string[] = ['imagen', 'descripcion', 'precio', 'cantidad', "total", "eliminar"];

  public listaProductos: any[] = null;
  public dsProductos:any[] = [];
  public total:number = 0;
  //public productoEnCarrito: number = 0;
  public bodegaURL:string = config.API_BASE_URL_BODEGA;
  public anwoURL:string = config.API_BASE_URL_ANWO;

  constructor(
    private router:Router, 
    private utils:UtilsService, 
    private productoService:ProductoService) { }

  ngOnInit(): void {

    this.listarProductos();

  }

  irAProductos(){
    this.router.navigate(['productos']);
  }

  listarProductos(){
    this.utils.mostrarSpinner();
    this.listaProductos = JSON.parse(localStorage.getItem("carrito")) ;
    if(this.listaProductos){
      this.listaProductos.forEach(pro => {
        pro.sinStock = false;
      });
    }

    this.dsProductos = this.listaProductos;

    if(!this.dsProductos){
      this.dsProductos = [];
    }
    this.calcularTotal(this.dsProductos);
    this.utils.ocultarSpinner();
  }

  calcularTotal(productos){

    this.total = 0;
    productos.forEach(element => {
      this.total = this.total + element.precio * element.cantidadCarrito;
      //this.productoEnCarrito += parseInt(element.cantidadCarrito);
      
    });
  }

  reducirCantidad(element, cant){

    if( cant.value > 1){
      cant.value = Number(cant.value) - 1;
      element.sinStock = false;
      this.utils.restarProductosCarrito$.emit(1);
    }
    element.cantidadCarrito = cant.value;
    
    this.calcularTotal(this.dsProductos);
    this.guardarCarrito();
  };

  aumentarCantidad(element, cant){

    if( cant.value < element.stock){
      cant.value = Number(cant.value) + 1;
      this.utils.sumarProductosCarrito$.emit(1);
      if ( cant.value == element.stock){
        element.sinStock = true;
      }
    } 
    element.cantidadCarrito = cant.value;
    
    this.calcularTotal(this.dsProductos);
    this.guardarCarrito();
  };

  guardarCarrito(){
    localStorage.setItem( "carrito", JSON.stringify(this.dsProductos) );
  }

  quitarProducto(pro){

    this.listaProductos = JSON.parse(localStorage.getItem("carrito"));
    let productoObtenido = this.listaProductos.find(x => x._id == pro._id)
    this.utils.restarProductosCarrito$.emit(Number(productoObtenido.cantidadCarrito));
    let i = this.listaProductos.indexOf(productoObtenido);
    this.listaProductos.splice( i, 1);
    localStorage.setItem( "carrito", JSON.stringify(this.listaProductos) );
    this.listarProductos();
  }

  generarCompra(){
    this.utils.mostrarSpinner();
    this.listaProductos = JSON.parse(localStorage.getItem("carrito"));
    console.log('lista desde local storage', this.listaProductos);

    this.listaProductos.forEach(producto => {

      producto.stock = producto.stock - producto.cantidadCarrito;

      this.productoService.actualizarProducto(producto).subscribe( data => {

        if(data.status == 201){

          this.utils.messageGood('Pago realizado con éxito. Gracias por preferirnos');
          this.resetearCarrito();
        }
      }, error => {
        console.log(error);
        this.utils.messageBad("Error al actualizar producto");
        this.utils.ocultarSpinner();
      });
      
    });
    
    this.utils.ocultarSpinner();
  }

  resetearCarrito(){
    localStorage.clear();
    location.reload();
  }
}
