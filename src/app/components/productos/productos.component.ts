import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../Services/producto/producto.service';
import { UtilsService } from '../../Services/utils/utils.service';
import { config } from '../../../environments/environment';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  public productos:any;
  public productosANWO:any;
  public bodegaURL:string = config.API_BASE_URL_BODEGA;
  public anwoURL:string = config.API_BASE_URL_ANWO;

  constructor(private productoService:ProductoService, private utils:UtilsService) { }

  ngOnInit(): void {

    this.utils.mostrarSpinner();
    this.productoService.obtenerProductosDesdeBodegaYANWO().subscribe( data => {

      this.productos = data['productosVigentes'];

      this.productos.forEach(producto => {

        if(producto.stock == 0){
          producto.sinStock = true;
          producto.sinStockBD = true;
        }else{
          producto.sinStock = false;
          producto.sinStockBD = false;
        }
        producto.esUno = true;
      });

      console.log('productos desde bodega y ANWO', this.productos);
      this.utils.ocultarSpinner();
    }, error => {
      console.log(error);
      this.utils.ocultarSpinner();
    });

  }

  reducirCantidad(cant,cantm,pro){
    if( cant.value > 1){
      cant.value = Number(cant.value) - 1;
      cantm.value = Number(cantm.value) - 1;
      pro.sinStock = false;
      if(cant.value == 1){
        pro.esUno = true;
      }else{
        pro.esUno = false;
      }
      
    }else{
      pro.esUno = true;
    }
  }

  aumentarCantidad(cant,cantm,producto){
    cant.value = Number(cant.value) + 1;
    cantm.value = Number(cantm.value) + 1;
    if(cant.value == 1){
      producto.esUno = true;
    }else{
      producto.esUno = false;
    }
    this.validarStock(producto, cant)
  }

  validarStock(producto, cant){

    let cantidad: number = Number(cant.value);
    let cantidadCarrito: number = this.obtenerCantidadCarritoProducto(producto);
    let cantidadTotal: number = cantidad + cantidadCarrito;
    let stock: number = Number(producto.stock);

    if (cantidadTotal >= stock){
      producto.sinStock = true;
    }
    if (cantidadTotal > stock){
      cant.value = Number(stock - cantidadCarrito)
      if(cant.value == 0){
        cant.value = 1;
      }
    }
  }

  obtenerCantidadCarritoProducto(producto){
    let productoLStorage = this.buscarProducto(producto._id);
    let cantidadCarrito: number;
    if(productoLStorage){
      cantidadCarrito = Number(productoLStorage.cantidadCarrito);
    }else{
      cantidadCarrito = 0;
    }
    return Number(cantidadCarrito);
  }

  buscarProducto(_id){
    
    let listaObtenida = JSON.parse(localStorage.getItem("carrito"));
    if(!listaObtenida) return null;
    let productoObtenido = listaObtenida.find(x => x._id == _id) 
    return productoObtenido;
  }

 agregarProducto(producto, cant){
    
    let cantidadCarrito = this.obtenerCantidadCarritoProducto(producto);
    let cantidadTotal: number;
    cantidadTotal = Number(cant.value) + Number(cantidadCarrito);
    this.validarStock(producto, cant);
    
    if(producto.stock >= cantidadTotal){

      producto.cantidadCarrito = 0;
      producto.cantidadCarrito = cant.value;
      this.utils.sumarProductosCarrito$.emit( Number(cant.value));

      let listaNueva = [];

      let listaObtenida = JSON.parse(localStorage.getItem("carrito")) ;

      if(listaObtenida){

        if(this.buscarProducto(producto._id) == null ){
          listaObtenida.push(producto);
          localStorage.setItem("carrito", JSON.stringify(listaObtenida) );
        }else{
          let productoEncontrado = this.buscarProducto(producto._id)
          productoEncontrado.cantidadCarrito = Number(productoEncontrado.cantidadCarrito) + Number(cant.value);
          
          this.quitarProducto(productoEncontrado);
          let listaPorActualizar = JSON.parse(localStorage.getItem("carrito")) ;
          listaPorActualizar.push(productoEncontrado);
          localStorage.setItem("carrito", JSON.stringify(listaPorActualizar) );
        }
        
      }else{
        listaNueva.push(producto);
        localStorage.setItem("carrito", JSON.stringify(listaNueva) );
      }

      this.utils.messageGood("Agregó " + cant.value + " " + producto.nombre + " al carrito de compras");

    }else{
      this.utils.messageBad("La cantidad seleccionada supera el stock del producto");
    }
  }

  quitarProducto(producto){

    let listaObtenida = JSON.parse(localStorage.getItem("carrito"));
    let productoObtenido = listaObtenida.find(x => x._id == producto._id)
    let i = listaObtenida.indexOf(productoObtenido);
    listaObtenida.splice( i, 1);
    localStorage.setItem( "carrito", JSON.stringify(listaObtenida) );
  }
}
